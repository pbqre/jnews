from django.db import models
from django.contrib.auth.models import User
from markdown2 import Markdown
from ckeditor.fields import RichTextField

class News(models.Model):
  user = models.ForeignKey(User, on_delete=models.CASCADE)
  headline = models.CharField(max_length=100)
  content = RichTextField(max_length=2000)
  image = models.ImageField(upload_to='news/')
  time = models.DateTimeField(auto_now_add=True, null=True)

  # def save(self, *args, **kwargs):
  #   m = Markdown()
  #   self.content = m.convert(self.content)
  #   super().save(*args, **kwargs)

  def __str__(self):
    return self.headline
