from django.http.response import Http404, HttpResponse
from django.shortcuts import redirect, render
from news.models import News
from django.views.generic import TemplateView, View
from django.views.generic import ListView
from django.views.generic import DetailView

from news.forms import NewsCreationForm

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

@method_decorator([login_required], name='dispatch')
class NewsCreateView(View):
  def get(self, request):
    context = {
      'form': NewsCreationForm()
    }
    return render(request, 'news/create-news.html', context=context)
  
  def post(self, request):
    form = NewsCreationForm(request.POST, files=request.FILES)
    if form.is_valid():
      object = form.save(commit=False)
      object.user = request.user
      object.save()
      return redirect('home')
    else:
      return HttpResponse("Error while creatign.")

class NewsListView(ListView):
  model = News
  context_object_name = 'newss'



def home(request):
  context = {
    'news': News.objects.all()
  }
  return render(request, 'news/home.html', context=context)

@method_decorator([login_required], name='dispatch')
class ShowNews(DetailView):
  model = News

class ShowNewsView(View):
  template_name = 'news/show-news.html'

  def get(self, request, id):
    try:
      news = News.objects.get(id=id)
    except News.DoesNotExist:
      return Http404('error while loading the model.')
    
    return render(request, self.template_name, context = {'news': news})
  
  def post(self, request):
    pass
