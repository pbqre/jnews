from django.urls import path
from django.views.generic.base import View
from news import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
  path('', views.home, name='home'),
  path('news/<int:id>', views.ShowNewsView.as_view(), name='show-news'),
  path('all-news/', views.NewsListView.as_view(), name='all-news'),
  path('show-news/<int:pk>', views.ShowNews.as_view(), name='show-news-view'),
  path('create-news', views.NewsCreateView.as_view(), name='create-news'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)