## Django Tutorial 3

### Topics
- [x] Generic Views. (TemplateView, View)
- [ ] Message Framework.
- [x] How to connect to a database server.
  - `pip install psycopg2-binary`
  ```py
    DATABASES = {
      'default': {
          'ENGINE': 'django.db.backends.postgresql',
          'NAME': '---------',
          'HOST': '---------',
          'USER': '---------',
          'PORT': 5432,
          'PASSWORD': '--------'
      }
  }
  ```
- Customizing admin panel.


### TODO
- [ ] Create form to add news.
- [ ] Heroku


### Your Task
- [x] Create a form to add news.
- [ ] Create Update form to update news.
- [ ] Add ability to delete news.